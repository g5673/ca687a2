#!/usr/bin/python


"""

Custom topology
for CA687 Assignment 2

"""

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import setLogLevel
from mininet.cli import CLI
from mininet.node import OVSSwitch, Controller, RemoteController
from mininet.link import TCLink

class Ca687Topo(Topo):

    def build(self):
        s1 = self.addSwitch('s1') #s1 represents the switch on the first floor
        h1 = self.addHost('h1', mac="00:00:00:00:00:01", ip="10.0.0.1/24") #h1 - h4 are the workstations of our first floor employees 
        h2 = self.addHost('h2', mac="00:00:00:00:00:02", ip="10.0.0.2/24")
        h3 = self.addHost('h3', mac="00:00:00:00:00:03", ip="10.0.0.3/24")
        h4 = self.addHost('h4', mac="00:00:00:00:00:04", ip="10.0.0.4/24")

        s2 = self.addSwitch('s2') #s1 represents the switch on the first floor
        h5 = self.addHost('h5', mac="00:00:00:00:00:05", ip="10.0.0.5/24")
        h6 = self.addHost('h6', mac="00:00:00:00:00:06", ip="10.0.0.6/24")
        h7 = self.addHost('h7', mac="00:00:00:00:00:07", ip="10.0.0.7/24")
        h8 = self.addHost('h8', mac="00:00:00:00:00:08", ip="10.0.0.8/24")


        s3 = self.addSwitch('s3')#s1 represents the switch on the first floor
        h9 = self.addHost('h9', mac="00:00:00:00:00:09", ip="10.0.0.9/24")
        h10 = self.addHost('h10', mac="00:00:00:00:00:10", ip="10.0.0.10/24")
        h11 = self.addHost('h11', mac="00:00:00:00:00:11", ip="10.0.0.11/24")
        h12 = self.addHost('h12', mac="00:00:00:00:00:12", ip="10.0.0.12/24")


        s4 = self.addSwitch('s4') #s4 is the switch that the switches from each floor are connected to

        s5 = self.addSwitch('s5') #s5 is the switch that the two servers are connected to
        h13 = self.addHost('h13', mac="00:00:00:00:00:13", ip="10.0.0.13/24") # h13 and h14 are our two servers
        h14 = self.addHost('h14', mac="00:00:00:00:00:14", ip="10.0.0.14/24")


        self.addLink(h1, s1) #link first floor workstations to Switch 1
        self.addLink(h2, s1)
        self.addLink(h3, s1)
        self.addLink(h4, s1)

        self.addLink(h5, s2) #link second floor workstations to Switch 2
        self.addLink(h6, s2)
        self.addLink(h7, s2)
        self.addLink(h8, s2)

        self.addLink(h9, s3) #link third floor workstations to Switch 3
        self.addLink(h10, s3)
        self.addLink(h11, s3)
        self.addLink(h12, s3)

        self.addLink(s1, s4) #link switches from each floor to Switch 4
        self.addLink(s2, s4)
        self.addLink(s3, s4)
        
        self.addLink(s4, s5) #link Switch 4 to Switch 5
        self.addLink(h13, s5) #link the two servers to Switch 5
        self.addLink(h14, s5) 



if __name__ == '__main__':
    setLogLevel('info')
    topo = Ca687Topo()
    c1 = RemoteController('c1', ip='127.0.0.1')
    net = Mininet(topo=topo, controller=c1)
    net.start()
    net.pingAll(timeout=0.5)
    CLI(net)
    net.stop()
